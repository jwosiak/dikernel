﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIKernel
{
    class InstancesConstructorWrapper<T> where T : class
    {
        public delegate object ConstructInstanceDelegate();
        ConstructInstanceDelegate instanceConstructor;
        public bool Singleton { get; private set; }

        public InstancesConstructorWrapper(bool Singleton, ConstructInstanceDelegate instanceConstructor)
        {
            this.Singleton = Singleton;
            this.instanceConstructor = instanceConstructor;
        }

        T instance;
        public T GetInstance()
        {
            if (Singleton)
            {
                if (instance == null)
                {
                    instance = (T)instanceConstructor.Invoke();
                }
                return instance;
            }

            return (T)instanceConstructor.Invoke();
        }
    }
}
