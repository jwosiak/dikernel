﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIKernel
{
    public class SimpleContainer
    {
        Dictionary<Type, object> registeredInstances = new Dictionary<Type, object>();

        public void RegisterType<T>(bool Singleton) where T : class, new()
        {
            registeredInstances.Add(typeof(T),
                new InstancesConstructorWrapper<T>(Singleton, () => new T()) );
        }

        public void RegisterType<From, To>(bool Singleton) 
            where To : class, From, new()
            where From : class
        {
            registeredInstances.Add(typeof(From),
                new InstancesConstructorWrapper<From>(Singleton, () => new To()) );
        }

        public T Resolve<T>() where T : class
        {
            try
            {
                return ((InstancesConstructorWrapper<T>)registeredInstances[typeof(T)]).GetInstance();
            }
            catch (KeyNotFoundException exc)
            {
                if (typeof(T).IsInterface || typeof(T).IsAbstract)
                {
                    throw new NotRegisteredDependencyException();
                }
                object obj = new object();
                ConstructorInfo constructor = typeof(T).GetConstructor(new Type[] { });
                return (T)constructor.Invoke(null);
            }
            
        }

    }

}
