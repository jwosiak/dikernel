﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DIKernel;

namespace DIKernelTests
{
    [TestClass]
    public class UnitTest1
    {
        interface IFoo
        {

        }

        class Foo : IFoo
        {

        }

        [TestMethod]
        public void TestMethod1()
        {
            SimpleContainer c = new SimpleContainer();
            Foo fs = c.Resolve<Foo>();
            Assert.AreNotEqual(null, fs);
        }

        [TestMethod]
        public void TestMethod2()
        {
            SimpleContainer c = new SimpleContainer();
            c.RegisterType<IFoo, Foo>(false);
            var fs = c.Resolve<IFoo>();
            Assert.AreEqual(new Foo().GetType(), fs.GetType()) ;
        }

        abstract class Bar
        {

        }

        [TestMethod]
        public void TestMethod3()
        {
            SimpleContainer c = new SimpleContainer();
            Assert.ThrowsException<NotRegisteredDependencyException>( () => c.Resolve<Bar>());
        }

        [TestMethod]
        public void TestMethod4()
        {
            SimpleContainer c = new SimpleContainer();
            try
            {
                var fs = c.Resolve<IFoo>();
                Assert.Fail();
            }
            catch (NotRegisteredDependencyException exc)
            {
            }
        }

        [TestMethod]
        public void TestMethod5()
        {
            SimpleContainer c = new SimpleContainer();

            c.RegisterType<Foo>(true);
            Foo f1 = c.Resolve<Foo>();
            Foo f2 = c.Resolve<Foo>();

            Assert.AreSame(f1, f2);
        }

        [TestMethod]
        public void TestMethod6()
        {
            SimpleContainer c = new SimpleContainer();

            c.RegisterType<IFoo, Foo>(true);
            IFoo f1 = c.Resolve<IFoo>();
            IFoo f2 = c.Resolve<IFoo>();

            Assert.AreSame(f1, f2);
        }

        class FooNoConstructor
        {
            private FooNoConstructor() { }
        }

        [TestMethod]
        public void TestMethod7()
        {
            SimpleContainer c = new SimpleContainer();
            try
            {
                FooNoConstructor f = c.Resolve<FooNoConstructor>();
                Assert.Fail();
            }
            catch (NullReferenceException exc)
            {
            }
            
        }
    }
}
